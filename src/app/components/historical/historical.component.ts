import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-historical',
  templateUrl: './historical.component.html',
  styleUrls: ['./historical.component.css']
})
export class HistoricalComponent implements OnInit {
  chart = [];
  barchart = [];
  control = [85.6, 1024, 1974.4, 19080.6];
  coal = [0, 13000, 26200, 263800];
  gas: number[];
  date = ['Initial', '6 month', '1 year', '10 year'];
  constructor() { }

  ngOnInit() {
    this.renderChart();
  }
  renderChart() {
    this.renderLine();
    this.renderBar();
    this.renderBarStack();
  }
  renderLine() {
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.date,
        datasets: [
          {
            label: 'ReCo Cost',
            data: this.control,
            borderColor: '#9C27B0',
            fill: false
          },
          {
            label: 'Coal savings',
            data: this.coal,
            borderColor: '#ffcc00',
            fill: false
          },
        ]
      },
      options: {
        title: {
          display: true,
          text: 'ReCo cost vs Coal cost savings over time cost prediction.'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Cost in hundred thousands'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Timeline'
            }
          }],
        }
      }
    });
  }
  renderBarStack() {
    this.barchart = new Chart('bar', {
      type: 'bar',
      data: {
        labels: ['Lifetime'],
        datasets: [{
          label: 'ReCo',
          borderWidth: 1,
          data: [
            22164.6
          ],
          borderColor: '#9C27B0',
          backgroundColor: '#9C27B0',
          fill: true
        }, {
          label: 'Coal',
          borderWidth: 1,
          data: [
            303000
          ],
          borderColor: '#ffcc00',
          backgroundColor: '#ffcc00',
          fill: true
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Overall cost vs savings'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        legend: {
          display: true
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: true,
            scaleLabel: {
              display: true,
              labelString: 'Timeline'
            }
          }],
          yAxes: [{
            stacked: true,
            scaleLabel: {
              display: true,
              labelString: 'Cost in hundred thousands'
            }
          }]
        }
      }
    });
  }
  renderBar() {
    this.barchart = new Chart('bar2', {
      type: 'bar',
      data: {
        labels: ['6 month', '1 year', '10 years'],
        datasets: [{
          label: 'Coal',
          borderWidth: 1,
          data: [
            197422, 394843, 400000
          ],
          borderColor: '#9C27B0',
          backgroundColor: '#9C27B0',
          fill: true
        }, {
          label: 'Carbon Dioxide',
          borderWidth: 1,
          data: [
            564626, 1129253, 1130000
          ],
          borderColor: '#ffcc00',
          backgroundColor: '#ffcc00',
          fill: true
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'Overall environmental savings'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        legend: {
          display: true
        },
        responsive: true,
        scales: {
          xAxes: [{
            stacked: false,
            scaleLabel: {
              display: true,
              labelString: 'Timeline'
            }
          }],
          yAxes: [{
            stacked: false,
            scaleLabel: {
              display: true,
              labelString: 'Saving in tonnes'
            }
          }]
        }
      }
    });
  }
}
