import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-realtime',
  templateUrl: './realtime.component.html',
  styleUrls: ['./realtime.component.css']
})
export class RealtimeComponent implements OnInit {
  saving = 0;

  constructor() { }

  ngOnInit() {
    setInterval(() => {
      this.increment();
    }, 1000);
  }
  increment() {
    this.saving += this.randomIntFromInterval(4, 12);
  }
  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
