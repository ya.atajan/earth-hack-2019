import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RealtimeComponent} from './components/realtime/realtime.component';
import {HistoricalComponent} from './components/historical/historical.component';

const routes: Routes = [
  {path: 'historical', component: HistoricalComponent},
  {path: 'realtime', component: RealtimeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
